Algorytm dynamicznego load balancingu DWRS (Dynamic Weighted Random Selection)
====================================

Pseudokod
----------
````
Zmienne: 
serwery.Obciazenie - obciazenie kazdego z serwerow
serwery[i].IP - adres serwera
wagi_serwerow 
docelowy_IP 
suma_wag
losowa_liczba - liczba od 0 do sumy wag
v - zmienna pomocnicza opisujaca wartosc sumy obciazen dla danej iteracji
iterator

funkcja IP wybierz_serwer(nowy pakiet) {
	wagi_serwerow  = waga_serwera(serwery.Obciazenie)
If (nowy pakiet skierowany do naszej infrastruktury)
suma_wag = suma(wagi_serwerow) 
losowa_liczba = rand(0, suma_wag)
v = 0 
iterator = 0
		For each (serwer : serwery) {
			v += wagi_serwerow(iterator)
			If ( v >= losowa_liczba) {
				docelowy_IP = serwer.IP
				break 
			}
			iterator++
		}
return docelowy_IP
}
````

````
funkcja waga_serwera(serwery.Obciazenie){
	a = 0
	wagi_serwerow
	For each (obciazenie in serwery.Obciazenie){
		wagi_serwerow[a] = 100 - obciazenie
		a++
	}
	return wagi_serwerow
}
````
Topologia sieci
------------

![Topo](/screenshots/schemat_sieci.png)

Schemat architektury
-------------------

![Arch](/screenshots/arch.png)

Wykorzystane narzedzia
-----------------

- Floodlight
- Multi-Generator mgen
- Java
- Mininet
- Python
- Bash

Jak uruchomic
====================================

1. Uruchomic mininet.

2. Uruchomic kontroler: src/main/java/net.floodlightcontroller.core/Main.java

3. Zestawic tunel pomiedzy maszyne na ktorej jest wlaczony kontroler, a mininetem poleceniem:
	
	````
	ssh mininet@<mininet ip> -R 192.168.1.101:8080:localhost:8080 -N
	````
	
4. Uruchomic topologie w mininecie poleceniem:
	
	````
	sudo python topology.py <controller ip>
	````
	
5. Uruchomic tunel z maszyny minineta do kontrolera c0 poleceniem:
	
	````
	ssh mininet@localhost -L 0.0.0.0:8081:localhost:8080
	````

Autorzy
------------------------

- Dominik Stoch
- Mikolaj Gwiazdowicz
- Piotr Jurgala
- Szymon Stryczek


